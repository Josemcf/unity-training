﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCollision : MonoBehaviour {

    private Character character;

	// Use this for initialization
	void Start () {
        character = GetComponentInParent<Character>();
	}

    void OnCollisionStay2D(Collision2D col) {
        if (col.gameObject.tag == "Ground") {
            character.grounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D col) {
        if (col.gameObject.tag == "Ground") {
            character.grounded = false;
        }
    }
}
