﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public float speed = 20f;
    public float maxSpeed =  5f;
    public bool grounded;

    private Rigidbody2D rb2d;
    private Animator anim;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        anim.SetFloat("speedAnim",Mathf.Abs(rb2d.velocity.x));
        anim.SetBool("groundedAnim", grounded);
	}

    void FixedUpdate() {
        float h = Input.GetAxis("Horizontal");
        rb2d.AddForce(Vector2.right * speed * h);

        float limitSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
        rb2d.velocity = new Vector2(limitSpeed, rb2d.velocity.y);

        if (h > 0.1f) {
            transform.localScale = new Vector3(1f,1f,1f);
        }

        if (h < -0.1f) {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }

        /*if (rb2d.velocity.x > maxSpeed) {
            rb2d.velocity = new Vector2(maxSpeed,rb2d.velocity.y);
        }
        if (rb2d.velocity.x < -maxSpeed){
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }*/
    }
}
